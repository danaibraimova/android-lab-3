package com.example.user.lab3;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

// News is a class annotated with @Entity.
@Database(entities = { News.class }, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NewsDAO newsDao();
}

/* Instead of running queries on the database directly, you are highly recommended to create Dao classes.
Using Dao classes will allow you to abstract the database communication in a more logical layer
which will be much easier to mock in tests (compared to running direct sql queries).
 */