package com.example.user.lab3;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.user.lab3.News;

import java.util.List;

@Dao
public interface NewsDAO {

    @Query("SELECT * FROM news")
    List<News> getAll();

    @Insert
    void insert(News news);

    @Delete
    void delete(News news);

    @Query("SELECT * FROM news")
    List<News> getAllNews();
}