package com.example.user.lab3;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */

public class CategoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private CategoriesAdapter adapter;
    private List<Category> categoryList;

    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_category, container, false);
        // Inflate the layout for this fragment

        recyclerView = view.findViewById(R.id.recycler_view);

        categoryList = new ArrayList<>();
        adapter = new CategoriesAdapter(this.getContext(), categoryList);
        recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 2)); // Creates a vertical GridLayoutManager
        recyclerView.setItemAnimator(new DefaultItemAnimator()); // basic animations on remove, add, and move events that happen to the items in a RecyclerView. RecyclerView
        recyclerView.setAdapter(adapter);

        prepareCategories();

        return view;
    }

    /**
     * Adding few categories for testing
     */
    private void prepareCategories() {
        int[] covers = new int[] {
                R.drawable.news,
                R.drawable.finance,
                R.drawable.music,
                R.drawable.sport,
                R.drawable.culture, };

        Category a = new Category("News", 37, covers[0]);
        categoryList.add(a);

        a = new Category("Finance", 6, covers[1]);
        categoryList.add(a);

        a = new Category("Music", 9, covers[2]);
        categoryList.add(a);

        a = new Category("Sport", 5, covers[3]);
        categoryList.add(a);

        a = new Category("Culture", 5, covers[4]);
        categoryList.add(a);

        adapter.notifyDataSetChanged();
    }
}
